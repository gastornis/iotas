<?xml version="1.0" encoding="UTF-8"?>
<schemalist>
    <schema path="/org/gnome/gitlab/cheywood/Iotas/" id="@APP_ID@" gettext-domain="@GETTEXT_PACKAGE@">
        <key type="s" name="editor-theme">
            <default>'iotas-mono'</default>
            <summary>Theme used by editor view</summary>
            <description>The GtkSourceView style scheme id.</description>
        </key>
        <key type="b" name="first-start">
            <default>true</default>
            <summary>First start</summary>
            <description>Whether starting for the first time.</description>
        </key>
        <key type="i" name="font-size">
            <default>14</default>
            <summary>Font size</summary>
            <description>Font used in main editor view.</description>
        </key>
        <key type="i" name="line-length">
            <default>800</default>
            <summary>Line length</summary>
            <description>Line length used in both the editor and markdown render views (pixels).</description>
            <range min="50" max="100000"/>
        </key>
        <key type="b" name="use-monospace-font">
            <default>true</default>
            <summary>Whether to use a monospace font</summary>
            <description>The specific fonts are sourced from GNOME's monospace and document font settings.</description>
        </key>
        <key type="b" name="markdown-render-enabled">
            <default>true</default>
            <summary>Markdown render view support</summary>
            <description>Whether to support showing markdown render view (a temporary performance concession for a WebKit issue).</description>
        </key>
        <key type="b" name="markdown-syntax-detection-enabled">
            <default>true</default>
            <summary>Markdown syntax detection</summary>
            <description>Whether to detect markdown syntax. Required for syntax highlighting and formatting (toolbar and shortcuts).</description>
        </key>
        <key type="b" name="markdown-keep-webkit-process">
            <default>true</default>
            <summary>Markdown WebKit process retention.</summary>
            <description>When enabled the WebKit process is retained between uses of the render view, decreasing load time and increasing memory usage.</description>
        </key>
        <key type="b" name="markdown-tex-support">
            <default>false</default>
            <summary>Markdown TeX support for maths equations.</summary>
            <description>Whether to support rendering maths equations. Slightly increases markdown render time.</description>
        </key>
        <key type="b" name="markdown-use-monospace-font">
            <default>false</default>
            <summary>Markdown monospace font.</summary>
            <description>Whether to use a monospace font for the markdown render.</description>
        </key>
        <key type="b" name="markdown-default-to-render">
            <default>false</default>
            <summary>Markdown default to render.</summary>
            <description>Whether to show the render view when opening the note.</description>
        </key>
        <key type="d" name="markdown-render-proportional-to-monospace-font-ratio">
            <default>0.8</default>
            <summary>Ratio adjusting proportional to monospace font in markdown render</summary>
            <description>A value of 1 will result in no adjustment.</description>
            <range min="0.05" max="10"/>
        </key>
        <key type="s" name="nextcloud-endpoint">
            <default>''</default>
            <summary>Nextcloud server to sync against</summary>
            <description>Path to the Nextcloud sync instance.</description>
        </key>
        <key type="s" name="nextcloud-username">
            <default>''</default>
            <summary>Username for the Nextcloud sync server</summary>
            <description>Login to use with the Nextcloud sync instance.</description>
        </key>
        <key type="i" name="nextcloud-prune-threshold">
            <default>0</default>
            <summary>Prune threshold for Nextcloud sync</summary>
            <description>Used to reduce the number of records pulled from the Nextcloud server during sync.</description>
        </key>
        <key type="d" name="network-timeout">
            <default>10</default>
            <summary>Network timeout in seconds</summary>
            <description>Applied as both the connection and read timeout for each request.</description>
            <range min="0.05" max="3600"/>
        </key>
        <key type="b" name="show-startup-secret-service-failure">
            <default>true</default>
            <summary>Whether to show a message on Secret Service failure</summary>
            <description>Disabling this provides for a cleaner startup if never intending to use Nextcloud Notes sync. on a device without a Secret Service provider.</description>
        </key>
        <key type="b" name="show-syncing-debug-notification">
            <default>false</default>
            <summary>Whether to show a notification when syncing from the server</summary>
            <description>Useful for increasing visibility of network issues.</description>
        </key>
        <key type="b" name="spelling-enabled">
            <default>true</default>
            <summary>Spelling enabled</summary>
            <description>Whether spell checking is enabled.</description>
        </key>
         <key type="s" name="spelling-language">
            <default>''</default>
            <summary>Spelling language</summary>
            <description>Language tag to attempt to use by default for spelling.</description>
        </key>
        <key name="style-variant" type="s">
            <choices>
                <choice value="follow"/>
                <choice value="light"/>
                <choice value="dark"/>
            </choices>
            <default>'follow'</default>
            <summary>Colour scheme style</summary>
            <description>Choosing to follow desktop colour style or enforce dark or light visuals.</description>
        </key>
        <key type="i" name="sync-interval">
            <default>10</default>
            <summary>Sync interval</summary>
            <description>Interval between pulling from the Nextcloud sync server (seconds).</description>
        </key>
        <key type="ai" name="window-size">
            <default>[1000, 700]</default>
            <summary>Window size</summary>
            <description>Remember the window size.</description>
        </key>
        <key type="b" name="persist-sidebar">
            <default>true</default>
            <summary>Whether to persist the index sidebar in large windows</summary>
            <description>Whether to the index sidebar is permanently present in large windows (generally desktop).</description>
        </key>
        <key name="editor-header-bar-visibility" type="s">
            <choices>
                <choice value="always-visible"/>
                <choice value="auto-hide"/>
                <choice value="auto-hide-fullscreen-only"/>
            </choices>
            <default>'always-visible'</default>
            <summary>Editor header bar visibility</summary>
        </key>
        <key name="editor-formatting-bar-visibility" type="s">
            <choices>
                <choice value="always-visible"/>
                <choice value="auto-hide"/>
                <choice value="auto-hide-fullscreen-only"/>
                <choice value="disabled"/>
            </choices>
            <default>'always-visible'</default>
            <summary>Formatting bar visibility</summary>
            <description>Depends on syntax detection.</description>
        </key>
        <key type="s" name="backup-note-extension">
            <default>'txt'</default>
            <summary>File extension for backed up notes</summary>
            <description>File extension added to each note when exporting for backup.</description>
        </key>
        <key type="s" name="index-category-style">
            <choices>
                <choice value="monochrome"/>
                <choice value="muted"/>
                <choice value="blue"/>
                <choice value="green"/>
                <choice value="yellow"/>
                <choice value="orange"/>
                <choice value="red"/>
                <choice value="purple"/>
                <choice value="none"/>
            </choices>
            <default>'monochrome'</default>
            <summary>Index category style</summary>
            <description>Style options for category labels on index rows.</description>
        </key>
        <key type="s" name="last-launched-version">
            <default>''</default>
            <summary>Last launched version</summary>
            <description>The last version of the app that was run. Shouldn't be edited.</description>
        </key>
        <key type="s" name="last-export-directory">
            <default>''</default>
            <summary>Directory last used for export</summary>
            <description>Only applies to instances with filesystem access. Shouldn't be edited.</description>
        </key>
        <key type="s" name="extra-export-formats">
            <default>''</default>
            <summary>Extra pandoc formats for exporting</summary>
            <description>See README for definition.</description>
        </key>
        <key type="b" name="show-extended-preferences">
            <default>false</default>
            <summary>Whether to show extended preferences</summary>
            <description>When enabled an excessive number of preferences are shown.</description>
        </key>
        <key type="b" name="markdown-syntax-highlighting-enabled">
            <default>true</default>
            <!-- holding for a period to allow for migration -->
            <summary>UNUSED, migrated old preference</summary>
        </key>
        <key type="b" name="auto-hide-editor-headerbar">
            <default>false</default>
            <!-- holding for a period to allow for migration -->
            <summary>UNUSED, migrated old preference</summary>
        </key>
        <key type="b" name="hide-editor-headerbar-when-fullscreen">
            <default>true</default>
            <!-- holding for a period to allow for migration -->
            <summary>UNUSED, migrated old preference</summary>
        </key>
    </schema>
</schemalist>
