# Iotas note taking

Iotas aims to provide distraction-free note taking via its mobile-first design.

<img src="https://gitlab.gnome.org/World/iotas/-/raw/main/data/screenshots/mobile.png" width="350px" />

## Featuring

- Optional speedy sync with Nextcloud Notes
- Offline note editing, syncing when back online
- Category editing and filtering
- Favourites
- Spell checking
- Search within the collection or individual notes
- Focus mode and optional hiding of the editor header and formatting bars
- In preview: export to PDF, ODT and HTML
- A convergent design, seeing Iotas as at home on desktop as mobile
- Search from GNOME Shell
- Note backup and restoration (from CLI, for using without sync)
- The ability to change font size and toggle monospace style

Writing in markdown is supported but optional, providing

- Formatting via toolbar and shortcuts
- Syntax highlighting with themes
- Formatted render view
- The ability to check off task lists from the rendered markdown

Slightly more technical details, for those into that type of thing

- Nextcloud Notes sync is via the REST API, not WebDAV, which makes it snappy
- There's basic sync conflict detection
- Notes are constantly saved
- Large note collections are partially loaded to quicken startup 
- Notes are stored in SQLite, providing for fast search (FTS) without reinventing the wheel. Plain files can be retrieved by making a backup (CLI).

## Install

<a href="https://flathub.org/apps/details/org.gnome.World.Iotas"><img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" /></a>

## Issues & contact

Please report any bugs you come across in [the usual place](https://gitlab.gnome.org/World/iotas/-/issues).

## FAQ / tips

Are [in a state of being reworked](https://gitlab.gnome.org/World/iotas/-/wikis/faq).

## Development

Iotas was conceived in response to the question: what can be used to write simple notes on mobile Linux with fast sync to a self-hosted FLOSS server?

It's fairly minimal by design, so changes to integrate complex functionality may not be merged. Probably best to drop a note to say hi (in an issue) if you're interested in working on something sizeable and don't want to risk wasted effort. Providing a good experience for the current generation of Linux mobile devices is a higher priority than a feature-rich desktop app.

Iotas follows the [GNOME Code of Conduct](https://conduct.gnome.org) for all communications and contributions.

### Authentication storage

Iotas uses the Secret Service to store authentication details for Nextcloud. This is typically handled smoothly by GNOME Keyring but that may not be the case on mobile or other environments. KWallet is [reported](https://gitlab.gnome.org/World/iotas/-/issues/106) to be working as of Iotas v0.2.7.

## Thanks

Much was learnt and borrowed from projects Secrets, Feeds, Lollypop, Apostrophe, Fractal, Text Editor, Paper and Nextcloud Notes for Android. Many thanks!

## Why "Iotas"?

An iota is a little bit and this app is designed for jotting down little things on little devices. Iota stems from the same Greek word as jot and is commonly used in negative statements eg. "not one iota of …", but we think the word has more to give. Maybe somebody will take note?
