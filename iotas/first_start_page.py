from gi.repository import Adw, GLib, Gtk

import logging

import iotas.config_manager
from iotas.config_manager import HeaderBarVisibility
from iotas.ui_utils import have_window_with_width, is_likely_mobile_device


@Gtk.Template(resource_path="/org/gnome/World/Iotas/ui/first_start_page.ui")
class FirstStartPage(Adw.Bin):
    __gtype_name__ = "FirstStartPage"

    def initialise_device(self) -> None:
        """Apply any device-specific initialisation."""

        # Another location (ala MigrationAssistant) using this pattern as a result of not yet
        # discovering the clean way to be notified when a window has obtained its initial size
        def wait_and_run():
            if not have_window_with_width():
                GLib.timeout_add(250, wait_and_run)
            else:
                self.__set_formatting_bar_visibility_based_on_device()

        wait_and_run()

    def __set_formatting_bar_visibility_based_on_device(self) -> None:
        if is_likely_mobile_device():
            # Default setting is always visible so no change
            logging.info("Likely on mobile device, setting formatting bar to stay visible")
        else:
            logging.info("Likely not on mobile device, setting formatting bar to auto hide")
            iotas.config_manager.set_editor_formatting_bar_visibility(HeaderBarVisibility.AUTO_HIDE)
