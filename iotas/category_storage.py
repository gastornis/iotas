from abc import ABC, abstractmethod
from typing import List

from iotas.category import Category


class CategoryStorage(ABC):
    """Category storage interface."""

    @abstractmethod
    def get_all_categories(self) -> List[Category]:
        """Fetch all categories from the database.

        :return: List of categories
        :rtype: List[Category]
        """
        raise NotImplementedError()
